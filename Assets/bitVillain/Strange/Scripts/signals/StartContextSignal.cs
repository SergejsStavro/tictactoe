﻿using strange.extensions.signal.impl;

namespace com.bitvillain.strange
{
    public class StartContextSignal : Signal { }
}