﻿using UnityEngine;

namespace com.bitvillain.services
{
    public class ResourceFileLoaderService : IFileLoaderService
    {
        public string LoadString(string filePath) 
        {
            var data = Resources.Load<TextAsset>(filePath);

            return data.text;
        }
    }
}