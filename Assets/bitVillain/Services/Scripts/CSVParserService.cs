﻿using System.Collections.Generic;
using System.Text;

namespace com.bitvillain.services
{
    public class CSVParserService
    {
        public List<string[]> GetRowsFromString(string data, bool ignoreFirst = false)
        {
            data = data.Replace("\r\n", "\n").Replace('\r', '\n');

            var rowsRaw = data.Split('\n');

            var rows = new List<string[]>();

            int startingRowIndex = ignoreFirst ? 1 : 0;

            for (var i = startingRowIndex; i < rowsRaw.Length; i++) // skip first line, it's a header
            {
                var columns = rowsRaw[i].Split(',');

                if (columns.Length > 1)
                    rows.Add(columns);
            }

            return rows;
        }
    }
}