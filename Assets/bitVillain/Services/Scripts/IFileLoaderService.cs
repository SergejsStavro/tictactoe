﻿namespace com.bitvillain.services
{
    public interface IFileLoaderService
    {
        string LoadString(string filePath);
    }
}