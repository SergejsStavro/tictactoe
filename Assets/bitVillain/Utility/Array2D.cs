namespace com.bitvillain.utility
{
    public class Array2D
    {
        public int[,] ConvolveWithinBounds(
            int[,] input,
            int[,] filter,
            int filterOffsetX = 0,
            int filterOffsetY = 0
        )
        {
            int yiTemp;
            int xiTemp;

            int yfLength = filter.GetLength(0);
            int xfLength = filter.GetLength(1);

            int yiLength = input.GetLength(0);
            int xiLength = input.GetLength(1);

            int yDelta = yiLength - yfLength + 1;
            int xDelta = xiLength - xfLength + 1;

            int[,] output = new int[yDelta, xDelta];

            for (int yi = 0; yi < yDelta; yi++)
            {
                for (int xi = 0; xi < xDelta; xi++)
                {
                    output[yi, xi] = 0;
                    for (int yf = 0; yf < yfLength; yf++)
                    {
                        for (int xf = 0; xf < xfLength; xf++)
                        {
                            yiTemp = yi + yf + filterOffsetY;
                            xiTemp = xi + xf + filterOffsetX;

                            if (yiTemp >= 0 && xiTemp >= 0 &&
                                yiTemp < yiLength && xiTemp < xiLength
                            )
                            {
                                output[yi, xi] += input[yiTemp, xiTemp] * filter[yf, xf];
                            }
                        }
                    }
                }
            }

            return output;
        }
    }
}