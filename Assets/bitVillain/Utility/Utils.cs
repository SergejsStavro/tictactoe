namespace com.bitvillain.utility
{
    public class Utils
    {
        #region enum utils    
        public static string GetEnumElementName<T>(T item)
        {
            return System.Enum.GetName(typeof(T), item);
        }

        public static T GetEnumElementFromName<T>(string name)
        {
            return (T)System.Enum.Parse(typeof(T), name);
        }
        #endregion
    }
}