﻿using strange.extensions.signal.impl;

namespace com.bitvillain.tictactoe
{
    public class MarkTileSignal : Signal<int> { }
}