﻿using System.Collections.Generic;
using strange.extensions.signal.impl;

namespace com.bitvillain.tictactoe
{
    public class TileMarkedSignal : Signal<KeyValuePair<int, int>> { }
}