﻿using strange.extensions.signal.impl;

namespace com.bitvillain.tictactoe
{
    public class HighlightGridTilesSignal : Signal<int[], int> { }
}