﻿using System.Collections.Generic;
using UnityEngine;
using strange.extensions.command.impl;

namespace com.bitvillain.tictactoe
{
    public class MarkRandomTileCommand : Command
    {
        [Inject]
        public TileGridModel TileGridModel { get; set; }

        [Inject]
        public MarkTileSignal MarkTileSignal { get; set; }

        public override void Execute()
        {
            var emptyTiles = new List<int>();
            for (int i = 0; i < TileGridModel.Tiles.Length; i++)
            {
                if(TileGridModel.Tiles[i] == 0)
                    emptyTiles.Add(i);
            }

            if(emptyTiles.Count > 0)
            {
                int tileIndex = emptyTiles[Random.Range(0, emptyTiles.Count)];

                MarkTileSignal.Dispatch(tileIndex);
            }
        }
    }
}