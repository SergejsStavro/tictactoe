﻿using System.Collections.Generic;
using strange.extensions.command.impl;

namespace com.bitvillain.tictactoe
{
    public class MarkTileCommand : Command
    {
        [Inject]
        public GameModel GameModel { get; set; }

        [Inject]
        public TileGridModel TileGridModel { get; set; }

        [Inject]
        public int TileIndex { get; set; }

        [Inject]
        public TileMarkedSignal TileMarkedSignal { get; set; }

        public override void Execute()
        {
            TileGridModel.Tiles[TileIndex] = GameModel.CurrentPlayerID;

            TileMarkedSignal.Dispatch(new KeyValuePair<int, int>(TileIndex, GameModel.CurrentPlayerID));
        }
    }
}