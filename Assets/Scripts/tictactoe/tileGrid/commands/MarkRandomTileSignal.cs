﻿using strange.extensions.signal.impl;

namespace com.bitvillain.tictactoe
{
    public class MarkRandomTileSignal : Signal { }
}