using System.Collections.Generic;

namespace com.bitvillain.tictactoe
{
    public class TileGridModel
    {
        public int[] Tiles;
        public int GridRows;
        public int GridColumns;

        public void Initialize(int gridRows = 3, int gridColumns = 3)
        {
            GridRows = gridRows;
            GridColumns = gridColumns;

            Tiles = new int[GridColumns * GridRows];
            for (int i = 0; i < Tiles.Length; i++)
                Tiles[i] = 0;
        }

        public int[,] GetTile2DMask(int filter)
        {
            int x;
            int y;
            int[,] tiles = new int[GridRows, GridColumns];
            for (int i = 0; i < Tiles.Length; i++)
            {
                x = i % GridColumns;
                y = i / GridColumns;
                tiles[y, x] = Tiles[i] == filter ? 1 : 0;
            }

            return tiles;
        }

        public int[] GetTileIndices(int filter)
        {
            List<int> result = new List<int>();

            for (int i = 0; i < Tiles.Length; i++)
                if(Tiles[i] == filter)
                    result.Add(i);

            return result.ToArray();
        }
    }
}