﻿using UnityEngine;
using strange.extensions.mediation.impl;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace com.bitvillain.ui
{
    
    [System.Serializable]
    class IDToSymbolEntity
    {
        [SerializeField]
        public int ID;
        [SerializeField]
        public string Symbol;
        [SerializeField]
        public Color Color;
    }

    public class TileGridView : View
    {
        [SerializeField]
        GridLayoutGroup gridLayoutGroup;

        [SerializeField]
        Transform content;
        
        [SerializeField]
        GameObject tilePrefab;

        [SerializeField]
        Button[] contentButtons;

        [SerializeField]
        List<IDToSymbolEntity> IDToSymbolEntities;

        public Action<int> TileClicked;



        override protected void OnEnable()
        {
            base.OnEnable();

            if(contentButtons != null)
                for (int i = 0; i < contentButtons.Length; i++)
                {
                    var k = i;
                    contentButtons[i].onClick.RemoveAllListeners();
                    contentButtons[i].onClick.AddListener(() => OnTileClicked(k));
                }
        }

        private void OnTileClicked(int tileIndex)
        {
            if (TileClicked != null)
                TileClicked(tileIndex);
        }

        internal void Initialize(int[] tiles, int columnCount)
        {
            gridLayoutGroup.constraintCount = columnCount;
            
            GameObject tile;
            contentButtons = new Button[tiles.Length];
            for (int i = 0; i < tiles.Length; i++)
            {
                tile = GameObject.Instantiate(tilePrefab);
                tile.transform.SetParent(content);
                tile.transform.localScale = Vector3.one;
                tile.name = string.Format("Button ({0})", i);

                contentButtons[i] = tile.GetComponent<Button>();
                var k = i;
                contentButtons[i].onClick.AddListener(() => OnTileClicked(k));

                Text text = contentButtons[i].transform.Find("Text").GetComponent<Text>();
                text.text = GetIDSymbol(tiles[i]);
            }
        }

        internal void HighlightTiles(int[] tiles, int playerID)
        {
            for (int i = 0; i < tiles.Length; i++)
            {
                contentButtons[tiles[i]].GetComponent<Image>().color =
                    GetIDColor(playerID);
            }
        }

        internal void SetTileSymbol(int tileIndex, int playerID)
        {
            Text text = contentButtons[tileIndex].transform.Find("Text").GetComponent<Text>();
            text.text = GetIDSymbol(playerID);

            contentButtons[tileIndex].interactable = false;
        }

        string GetIDSymbol(int playerID)
        {
            string result = string.Empty;
            for (int i = 0; i < IDToSymbolEntities.Count; i++)
            {
                if(IDToSymbolEntities[i].ID == playerID)
                    result = IDToSymbolEntities[i].Symbol;
            }

            return result;
        }

        Color GetIDColor(int playerID)
        {
            Color result = Color.green;
            for (int i = 0; i < IDToSymbolEntities.Count; i++)
            {
                if(IDToSymbolEntities[i].ID == playerID)
                    result = IDToSymbolEntities[i].Color;
            }

            return result;
        }

        override protected void OnDisable()
        {
            base.OnDisable();
            
            for (int i = 0; i < contentButtons.Length; i++)
                contentButtons[i].onClick.RemoveAllListeners();
        }
    }
}