﻿using System.Collections.Generic;
using com.bitvillain.tictactoe;
using strange.extensions.mediation.impl;

namespace com.bitvillain.ui
{
    public class TileGridMediator : Mediator
    {
        [Inject]
        public TileGridView View { get; set; }

        [Inject]
        public TileGridModel TileGridModel { get; set; }
        
        [Inject]
        public GameStartedSignal GameStartedSignal { get; set; }

        [Inject]
        public MarkTileSignal MarkTileSignal { get; set; }

        [Inject]
        public TileMarkedSignal TileMarkedSignal { get; set; }

        [Inject]
        public HighlightGridTilesSignal HighlightGridTilesSignal { get; set; }

        public override void OnRegister()
        {
            View.TileClicked += OnTileClicked;

            GameStartedSignal.AddListener(HandleTileGridStartedSignal);
            TileMarkedSignal.AddListener(HandleTileMarkedSignal);
            HighlightGridTilesSignal.AddListener(HandleHighlightGridTilesSignal);
        }

        private void HandleHighlightGridTilesSignal(int[] tileIndices, int playerID)
        {
            View.HighlightTiles(
                tileIndices,
                playerID
            );
        }

        private void HandleTileMarkedSignal(
            KeyValuePair<int, int> tileIndexToPayerID
        )
        {
            View.SetTileSymbol(
                tileIndexToPayerID.Key,
                tileIndexToPayerID.Value
            );
        }

        private void HandleTileGridStartedSignal()
        {
            View.Initialize(
                TileGridModel.Tiles,
                TileGridModel.GridColumns
            );
        }

        private void OnTileClicked(int tileIndex)
        {
            MarkTileSignal.Dispatch(
                tileIndex
            );
        }

        public override void OnRemove()
        {
            View.TileClicked -= OnTileClicked;

            GameStartedSignal.RemoveListener(HandleTileGridStartedSignal);
            TileMarkedSignal.RemoveListener(HandleTileMarkedSignal);
            HighlightGridTilesSignal.RemoveListener(HandleHighlightGridTilesSignal);
        }
    }
}