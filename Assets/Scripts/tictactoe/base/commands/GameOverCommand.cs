﻿using com.bitvillain.ui;
using DG.Tweening;
using strange.extensions.command.impl;

namespace com.bitvillain.tictactoe
{
    public class GameOverCommand : Command
    {
        [Inject]
        public GameModel GameModel { get; set; }

        [Inject]
        public TileGridModel TileGridModel { get; set; }

        [Inject]
        public int PlayerID { get; set; }
        
        [Inject]
        public SelectScreenSignal SelectScreenSignal { get; private set; }

        [Inject]
        public HighlightGridTilesSignal HighlightGridTilesSignal { get; set; }

        public override void Execute()
        {
            GameModel.WinnerID = PlayerID;

            if(GameModel.PlayerOrder.Contains(PlayerID))
            {
                //highlight all of the winner tiles
                HighlightGridTilesSignal.Dispatch(
                    TileGridModel.GetTileIndices(PlayerID),
                    PlayerID
                );
            }

		    Sequence sequence = DOTween.Sequence();
            sequence.AppendInterval(1);
            sequence.AppendCallback(
                () => {
                    SelectScreenSignal.Dispatch(ScreenFSMCommand.ToOver);
                }
            );
        }
    }
}