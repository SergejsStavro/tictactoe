﻿using strange.extensions.command.impl;

namespace com.bitvillain.tictactoe
{
    public class SwitchToNextPlayerCommand : Command
    {
        [Inject]
        public GameModel GameModel { get; set; }

        [Inject]
        public MarkRandomTileSignal MarkRandomTileSignal { get; set; }

        public override void Execute()
        {
            GameModel.CurrentPlayerOrderIndex++;
            if (GameModel.CurrentPlayerOrderIndex >= GameModel.PlayerOrder.Count)
                GameModel.CurrentPlayerOrderIndex = 0;

            if(GameModel.OpponentIDs.Contains(GameModel.CurrentPlayerID))
                MarkRandomTileSignal.Dispatch();
        }
    }
}