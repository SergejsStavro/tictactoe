﻿using System.Collections.Generic;
using com.bitvillain.utility;
using strange.extensions.command.impl;

namespace com.bitvillain.tictactoe
{
    public class CheckVictoryConditionsCommand : Command
    {
        [Inject]
        public Array2D Array2D { get; set; }

        [Inject]
        public GameModel GameModel { get; set; }

        [Inject]
        public TileGridModel TileGridModel { get; set; }

        [Inject]
        public KeyValuePair<int, int> TileKVP { get; set; }

        [Inject]
        public GameOverSignal GameOverSignal { get; set; }

        public override void Execute()
        {
            var playerID = TileKVP.Value;
            var tile2DMask = TileGridModel.GetTile2DMask(playerID);

            bool matchFound = false;

            for (int i = 0; i < GameModel.VictoryConditions.Count && !matchFound; i++)
            {
                var result = Array2D.ConvolveWithinBounds(
                    tile2DMask,
                    GameModel.VictoryConditions[i]
                );

                foreach (var item in result)
                    if (item == 3)
                    {
                        matchFound = true;
                        GameModel.WinningVictoryConditionIndices.Add(i);
                    }
            }

            if (GameModel.WinnerID == 0)
            {
                if (matchFound)
                {
                    GameOverSignal.Dispatch(playerID);
                }
                else
                {
                    var emptyTiles = new List<int>();
                    for (int i = 0; i < TileGridModel.Tiles.Length; i++)
                        if (TileGridModel.Tiles[i] == 0)
                            emptyTiles.Add(i);

                    if (emptyTiles.Count == 0)
                        GameOverSignal.Dispatch(0);
                }
            }
        }
    }
}