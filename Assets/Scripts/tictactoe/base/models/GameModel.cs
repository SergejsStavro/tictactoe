using System.Collections.Generic;

namespace com.bitvillain.tictactoe
{
    public class GameModel
    {
        public int PlayerID;
        public List<int> OpponentIDs;
        public List<int> PlayerOrder;
        public int CurrentPlayerOrderIndex;

        public List<int[,]> VictoryConditions;

        public List<int> WinningVictoryConditionIndices;

        public int WinnerID { get; set; }

        public int CurrentPlayerID
        {
            get
            {
                return PlayerOrder[CurrentPlayerOrderIndex];
            }
        }

        public void Initialize()
        {
            //TODO load data from a file
            PlayerOrder = new List<int> { 1, 2 };
            CurrentPlayerOrderIndex = 0;

            PlayerID = 1;
            OpponentIDs = new List<int>() { 2 };
            WinnerID = 0;

            VictoryConditions = new List<int[,]>();
            WinningVictoryConditionIndices = new List<int>();

            VictoryConditions.Add(
                new int[1, 3]{
                {1,1,1}
                }
            );

            VictoryConditions.Add(
                new int[3, 1]{
                {1},
                {1},
                {1}
                }
            );

            VictoryConditions.Add(
                new int[3, 3]{
                {1, 0, 0},
                {0, 1, 0},
                {0, 0, 1}
                }
            );

            VictoryConditions.Add(
                new int[3, 3]{
                {0, 0, 1},
                {0, 1, 0},
                {1, 0, 0}
                }
            );
        }
    }
}