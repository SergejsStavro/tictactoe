﻿using com.bitvillain.tictactoe;
using strange.extensions.mediation.impl;

namespace com.bitvillain.ui
{
    public class OverMediator : Mediator
    {
        [Inject]
        public OverView View{ get; set;}
        [Inject]
        public GameModel GameModel { get; set; }
        
        [Inject]
        public SelectScreenSignal SelectScreenSignal { get; private set; }

        public override void OnRegister()
        {
            View.ScreenButtonClicked += OnScreenButtonClicked;
            View.SetPlayerWonText(GameModel.WinnerID);
        }
        
        public override void OnRemove()
        {
            View.ScreenButtonClicked -= OnScreenButtonClicked;
        }

        private void OnScreenButtonClicked(ScreenFSMCommand screenFSMCommand)
        {
            SelectScreenSignal.Dispatch(screenFSMCommand);
        }
    }
}