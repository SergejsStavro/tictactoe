﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;

namespace com.bitvillain.ui
{
    public class OverView : View
    {
        [SerializeField]
        Text infoText;

        [SerializeField]
        Button[] contentButtons;

        [SerializeField]
        List<ScreenFSMCommand> screenFSMCommands;

        [SerializeField]
        string victoryTextFormat;

        [SerializeField]
        string drawText;

        public Action<ScreenFSMCommand> ScreenButtonClicked;

        override protected void OnEnable()
        {
            base.OnEnable();

            for (int i = 0; i < contentButtons.Length; i++)
            {
                var k = i;
                if(k < screenFSMCommands.Count)
                    contentButtons[i].onClick.AddListener(() => OnScreenButtonClicked(screenFSMCommands[k]) );
            }
        }

        override protected void OnDisable()
        {
            base.OnDisable();

            for (int i = 0; i < contentButtons.Length; i++)
                contentButtons[i].onClick.RemoveAllListeners();
        }

        private void OnScreenButtonClicked(ScreenFSMCommand screenFSMCommand)
        {
            if(ScreenButtonClicked != null)
                ScreenButtonClicked(screenFSMCommand);
        }

        public void SetPlayerWonText(int playerID)
        {
            infoText.text = 
                playerID != 0 ?
                    string.Format(victoryTextFormat, playerID) :
                        drawText;
        }
    }
}