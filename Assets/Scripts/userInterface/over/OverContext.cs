﻿using UnityEngine;
using com.bitvillain.strange;

namespace com.bitvillain.ui
{
    public class OverContext : SignalContext
    {
        public OverContext(MonoBehaviour view) : base(view) { }

        protected override void mapBindings()
        {
            base.mapBindings();

            mediationBinder.Bind<OverView>().To<OverMediator>();

            commandBinder
                .Bind<StartContextSignal>()
                .To<StartOverCommand>()
                .Once();
        }
    }
}