﻿using strange.extensions.context.impl;

namespace com.bitvillain.ui
{
    public class OverContextView : ContextView
    {
        void Awake()
        {
            context = new OverContext(this);
            context.Start();
        }
    }   
}
