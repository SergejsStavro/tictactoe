﻿using UnityEngine;
using com.bitvillain.strange;

namespace com.bitvillain.ui
{
    public class MenuContext : SignalContext
    {
        public MenuContext(MonoBehaviour view) : base(view) { }

        protected override void mapBindings()
        {
            base.mapBindings();

            mediationBinder.Bind<MenuView>().To<MenuMediator>();

            commandBinder
                .Bind<StartContextSignal>()
                .To<StartMenuCommand>()
                .Once();
        }
    }
}