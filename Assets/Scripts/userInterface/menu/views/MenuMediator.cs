﻿using strange.extensions.mediation.impl;

namespace com.bitvillain.ui
{
    public class MenuMediator : Mediator
    {
        [Inject]
        public MenuView View{ get; set;}
        
        [Inject]
        public SelectScreenSignal SelectScreenSignal { get; private set; }

        public override void OnRegister()
        {
            View.ScreenButtonClicked += OnScreenButtonClicked;
        }
        
        public override void OnRemove()
        {
            View.ScreenButtonClicked -= OnScreenButtonClicked;
        }

        private void OnScreenButtonClicked(ScreenFSMCommand screenFSMCommand)
        {
            SelectScreenSignal.Dispatch(screenFSMCommand);
        }
    }
}