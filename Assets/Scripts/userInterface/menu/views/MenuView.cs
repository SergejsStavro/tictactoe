﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;

namespace com.bitvillain.ui
{
    public class MenuView : View
    {
        [SerializeField]
        Button[] contentButtons;

        [SerializeField]
        List<ScreenFSMCommand> screenFSMCommands;

        public Action<ScreenFSMCommand> ScreenButtonClicked;

        override protected void OnEnable()
        {
            base.OnEnable();

            for (int i = 0; i < contentButtons.Length; i++)
            {
                var k = i;
                if(k < screenFSMCommands.Count)
                    contentButtons[i].onClick.AddListener(() => OnScreenButtonClicked(screenFSMCommands[k]) );
            }
        }

        override protected void OnDisable()
        {
            base.OnDisable();
            
            for (int i = 0; i < contentButtons.Length; i++)
                contentButtons[i].onClick.RemoveAllListeners();
        }

        private void OnScreenButtonClicked(ScreenFSMCommand screenFSMCommand)
        {
            if(ScreenButtonClicked != null)
                ScreenButtonClicked(screenFSMCommand);
        }
    }
}