﻿namespace com.bitvillain.ui
{
    public enum ScreenFSMState
    {
        None = 0,
        Menu = 1,
        Game = 2,
        Over = 3,
    }
}