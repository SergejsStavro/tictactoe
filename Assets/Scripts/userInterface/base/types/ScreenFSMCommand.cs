﻿namespace com.bitvillain.ui
{
    public enum ScreenFSMCommand
    {
        None = 0,
        ToMenu = 1,
        ToGame = 2,
        ToOver = 3,

        Back = 1000,
    }
}