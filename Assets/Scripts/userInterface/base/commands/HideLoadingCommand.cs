﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using strange.extensions.command.impl;
using strange.extensions.context.api;

public class HideLoadingCommand : Command
{
	[Inject(ContextKeys.CONTEXT_VIEW)]
	public GameObject contextView{get;set;}
	
	public override void Execute()
	{
		contextView.GetComponent<MonoBehaviour>().StartCoroutine(HideLoadingAsync());
	}
	
	public IEnumerator HideLoadingAsync()
	{
		yield return SceneManager.UnloadSceneAsync("LoadingScreen");
	}
}
