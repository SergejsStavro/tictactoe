﻿using strange.extensions.signal.impl;

namespace com.bitvillain.ui
{
    public class ScreenSelectedSignal : Signal<ScreenFSMState> { }
}