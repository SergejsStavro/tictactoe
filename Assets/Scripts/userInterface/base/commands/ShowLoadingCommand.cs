﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using UnityEngine.SceneManagement;

public class ShowLoadingCommand : Command
{
	[Inject(ContextKeys.CONTEXT_VIEW)]
	public GameObject contextView{get;set;}
	
	public override void Execute()
	{
		contextView.GetComponent<MonoBehaviour>().StartCoroutine(ShowLoadingAsync());
	}
	
	public IEnumerator ShowLoadingAsync()
	{
		yield return SceneManager.LoadSceneAsync("LoadingScreen");
	}
}
