﻿using strange.extensions.context.impl;

namespace com.bitvillain.ui
{
    public class UserInterfaceContextView : ContextView
    {
        void Awake()
        {
            context = new UserInterfaceContext(this);
            context.Start();
        }
    }
}