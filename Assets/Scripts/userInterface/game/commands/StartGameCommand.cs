﻿using com.bitvillain.tictactoe;
using strange.extensions.command.impl;

namespace com.bitvillain.ui
{
    public class StartGameCommand : Command
    {
        [Inject]
        public GameModel GameModel { get; set; }
        [Inject]
        public TileGridModel TileGridModel { get; set; }
        [Inject]
        public GameStartedSignal GameStartedSignal { get; set; }

        public override void Execute()
        {
            GameModel.Initialize();
            TileGridModel.Initialize();

            GameStartedSignal.Dispatch();
        }
    }
}