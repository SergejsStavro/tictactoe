﻿using strange.extensions.context.impl;

namespace com.bitvillain.ui
{
    public class GameContextView : ContextView
    {
        void Awake()
        {
            context = new GameContext(this);
            context.Start();
        }
    }   
}
