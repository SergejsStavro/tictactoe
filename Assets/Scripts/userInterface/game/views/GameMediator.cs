﻿using strange.extensions.mediation.impl;

namespace com.bitvillain.ui
{
    public class GameMediator : Mediator
    {
        [Inject]
        public GameView View { get; set; }

        [Inject]
        public SelectScreenSignal SelectScreenSignal { get; private set; }

        public override void OnRegister()
        {
            View.BackButtonClicked += OnBackButtonClicked;
        }

        public override void OnRemove()
        {
            View.BackButtonClicked -= OnBackButtonClicked;
        }

        private void OnBackButtonClicked()
        {
            SelectScreenSignal.Dispatch(ScreenFSMCommand.Back);
        }
    }
}