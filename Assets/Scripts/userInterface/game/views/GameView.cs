﻿using System;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;

namespace com.bitvillain.ui
{
    public class GameView : View
    {
        [SerializeField]
        Button backButton;

        public Action BackButtonClicked;
        
        override protected void OnEnable()
        {
            base.OnEnable();

            backButton.onClick.AddListener(OnBackButtonClicked);
        }

        override protected void OnDisable()
        {
            base.OnDisable();

            backButton.onClick.RemoveAllListeners();
        }

        private void OnBackButtonClicked()
        {
            if (BackButtonClicked != null)
                BackButtonClicked();
        }
    }
}