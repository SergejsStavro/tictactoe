﻿using UnityEngine;
using com.bitvillain.strange;
using com.bitvillain.tictactoe;

namespace com.bitvillain.ui
{
    public class GameContext : SignalContext
    {
        public GameContext(MonoBehaviour view) : base(view) { }

        protected override void mapBindings()
        {
            base.mapBindings();

            #region tile grid
            injectionBinder.Bind<TileGridModel>().ToSingleton();
                
            mediationBinder.Bind<TileGridView>().To<TileGridMediator>();

            injectionBinder.Bind<HighlightGridTilesSignal>().ToSingleton();
            injectionBinder.Bind<MarkRandomTileSignal>().ToSingleton();
            commandBinder
                .Bind<MarkTileSignal>()
                .To<MarkTileCommand>();
            
            commandBinder
                .Bind<MarkRandomTileSignal>()
                .To<MarkRandomTileCommand>();
            #endregion

            #region game
            mediationBinder.Bind<GameView>().To<GameMediator>();

            commandBinder
                .Bind<TileMarkedSignal>()
                .InSequence()
                .To<CheckVictoryConditionsCommand>()
                .To<SwitchToNextPlayerCommand>();

            commandBinder.Bind<GameOverSignal>().To<GameOverCommand>();
            #endregion

            injectionBinder.Bind<GameStartedSignal>().ToSingleton();

            commandBinder
                .Bind<StartContextSignal>()
                .To<StartGameCommand>()
                .Once();
        }
    }
}