﻿using com.bitvillain.strange;
using com.bitvillain.tictactoe;
using com.bitvillain.utility;
using UnityEngine;

namespace com.bitvillain.main
{
    public class MainContext : SignalContext
    {
        public MainContext(MonoBehaviour view) : base(view) { }

        protected override void mapBindings()
        {
            base.mapBindings();

            injectionBinder
                .Bind<Array2D>()
                .ToSingleton().CrossContext();

            injectionBinder
                .Bind<GameModel>()
                .ToSingleton().CrossContext();


            commandBinder
                .Bind<StartContextSignal>()
                .To<StartMainCommand>()
                .Once();
        }
    }
}