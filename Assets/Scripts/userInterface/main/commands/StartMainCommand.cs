﻿using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.context.api;

namespace com.bitvillain.main
{
    public class StartMainCommand : Command
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }

        public override void Execute() { }
    }
}